const app = getApp()
Component({
  data: {
    selected: 0,
    tabBar: {
      textColor: "#6C6C6C",
      selectedColor: "#1ECDB9",
      items: [
        {
          pagePath: "/pages/index/index",
          name: "候选人",
          icon: "/assets/img/xcx_b_apply_normal.png",
          activeIcon: "/assets/img/xcx_b_apply_active.png"
        },
        {
          pagePath: "/pages/imgCopper/index",
          name: "职位",
          icon: "/assets/img/xcx_b_job_normal.png",
          activeIcon: "/assets/img/xcx_b_job_active.png"
        },
        {
          pagePath: "/",
          name: "",
          icon: "https://qiniu-image.qtshe.com/miniapp_publish_job.png",
          activeIcon: "https://qiniu-image.qtshe.com/miniapp_publish_job.png"
        },
        {
          pagePath: "/pages/canvas/index",
          name: "消息",
          icon: "/assets/img/xcx_b_job_normal.png",
          activeIcon: "/assets/img/xcx_b_job_active.png"
        },
        {
          pagePath: "/pages/alipayMent/index",
          name: "我的",
          icon: "/assets/img/xcx_b_mine_normal.png",
          activeIcon: "/assets/img/xcx_b_mine_active.png"
        }
      ]
    }
  },
  methods: {
    switchTab(e) {
      const {dataset: { item: { pagePath = '' } } = {}} = e.target
      my.switchTab({
        url: pagePath
      })
    }
  }
});
