App({
  onLaunch(options) {
    console.log('App.js的onLaunch触发', options)
    my.setOptionMenu({
      icon: 'https://qiniu-image.qtshe.com/20240809tasklogo.png'
    })
  },
  onShow(options) {
    console.log('App.js的onShow触发', options)
    // 从后台被 scheme 重新打开
    // options.query == {number:1}
  },
  onHide(options) {
    console.log('App.js的onHide触发', options)
  },
  onOptionMenuClick() {
    my.setClipboard({
      text: 'https://b.qtshe.com/1Eu8Tf',
      success: () => {
        my.openOtherApp({
          url: 'weixin://'
        })
      }
    })
  },
  globalData: {
    tabbarIndex: 0
  }
})