Page({
  data: {
    autoplay: false, // 是否自动播放
    path: 'https://assets6.lottiefiles.com/packages/lf20_Tprkoc.json', // lottie动画数据源
    placeholder: 'https://qiniu-image.qtshe.com/default.png', // 版本过低时显示的缺省图
    optimize: true, // 允许低版本使用降级方案
    repeatCount: -1 // 是否重复播放，负数表示无限播放
  },
  onReady() {
    this.lottieContext = my.createLottieContext('lottieId');
  },
  // 开始播放
  handleStart() {
    this.lottieContext.play()
  },
  // 暂停播放
  handlePause() {
    this.lottieContext.pause()
  }
});