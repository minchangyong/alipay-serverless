Page({
  data: {},
  onLoad() {
   
  },
  handleCreatePoster() {
    my.showLoading({
      content: '生成中...'
    })
    this.setData({
      startInit: true
    }, () => {
      this.setData({
        imgDraw: {
          width: '750rpx',
          height: '1334rpx',
          background: 'https://qiniu-image.qtshe.com/20190506share-bg.png',
          views: [
            {
              type: 'image',
              url: 'https://qiniu-image.qtshe.com/1560248372315_467.jpg',
              css: {
                top: '32rpx',
                left: '30rpx',
                right: '32rpx',
                width: '688rpx',
                height: '420rpx',
                borderRadius: '16rpx'
              },
            },
            {
              type: 'image',
              url: 'https://qiniu-image.qtshe.com/default-avatar20170707.png',
              css: {
                top: '404rpx',
                left: '328rpx',
                width: '96rpx',
                height: '96rpx',
                borderWidth: '6rpx',
                borderColor: '#FFF',
                borderRadius: '96rpx'
              }
            },
            {
              type: 'text',
              text: '青团社-郝帅',
              css: {
                top: '532rpx',
                fontSize: '28rpx',
                left: '375rpx',
                align: 'center',
                color: '#3c3c3c'
              }
            },
            {
              type: 'text',
              text: `邀请您参与助力活动`,
              css: {
                top: '576rpx',
                left: '375rpx',
                align: 'center',
                fontSize: '28rpx',
                color: '#3c3c3c'
              }
            },
            {
              type: 'text',
              text: `宇宙最萌蓝牙耳机测评员`,
              css: {
                top: '644rpx',
                left: '375rpx',
                maxLines: 1,
                align: 'center',
                fontWeight: 'bold',
                fontSize: '44rpx',
                color: '#3c3c3c'
              }
            },
            {
              type: 'image',
              url: 'https://mass.alipay.com/wsdk/img?fileid=A*qwhmTo6OiEAAAAAAAAAAAAAAAQAAAQ&bz=am_afts_openhome&zoom=original.jpg',
              css: {
                top: '834rpx',
                left: '470rpx',
                width: '200rpx',
                height: '250rpx'
              }
            }
          ]
        }
      })
    })
  },
  onSuccess(res) {
    my.hideLoading()
    console.log(res.path)
    this.setData({
      imagePath: res.path
    })
  },
  onShareAppMessage() {
    return {
      title: 'xxx111',
      bgImgUrl: this.data.imagePath,
      scImgUrl: this.data.imagePath
    }
  },
  onError(err) {
    console.log(err)
    my.hideLoading()
    my.showToast({ content: '生成失败', icon: 'error' })
  },
  downloadImg() {
    my.saveImage({
      url: this.data.imagePath,
      success: () => {
        my.showToast({ content: '保存成功', icon: 'success' })
      },
      fail: () => {
        my.showToast({ content: '保存失败', icon: 'error' })
      }
    });
  },
  handleSaveImg() {
    my.getImageInfo({
      src: '../images/1.png',
      success: info => {
        console.log(info)
        my.saveImage({
          url: info.path,
          success: res => {
            console.log(res)
            my.showToast({
              content: '保存成功'
            })
          },
          fail: err => {
            console.log(err)
          }
        })
      },
      fail: err => {
        console.log(err)
      }
    })
  },
});
