Page({
  data: {
    targetUrl: ''
  },
  onLoad(options) {
    this.setData({
      targetUrl: decodeURIComponent(options.targetUrl)
    })
  },
  onMessage(e) {
    // const { appId = '' } = e.detail
    // if (appId) {
    //   my.navigateToMiniProgram({
    //     appId
    //   })
    // }
    my.getAuthCode({
      scopes: ['auth_user'],
      success: (result) => {
        console.log(result)
      },
      fail: (err) => {
        console.log(err)
      }
    })
  }
})
