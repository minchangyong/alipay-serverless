const app = getApp();
var appId = 20000078;
Page({
  data: {
    userId: "",
    accessToken: "",
    refreshToken: "",
    imgDraw: {},
    visible: false,
    navigateList: [
      {
        title: "test演示",
        url: "/pages/test/index"
      },
      {
        title: "生成海报分享图",
        url: "/pages/poster/index"
      },
      {
        title: "图片裁剪",
        url: "/pages/imgCopper/index"
      }
    ]
  },
  onLoad() {
    //   Object.keys(my).forEach(key => {
    //     console.log(key);
    //  })
  },
  handleDoing() {
    my.ap.navigateToAlipayPage({
      path: `https://render.alipay.com/p/s/i/?appId=20000067&url=https%3A%2F%2Frender.alipay.com%2Fp%2Fc%2F180020570000087316%2Findex.html`,
      complete: res => {
        console.log(res)
      }
    })
    
    // my.call("pushWindow", {
    //   url: 'alipays://platformapi/startapp?appId=20000067&url=https%3A%2F%2Frender.alipay.com%2Fp%2Fs%2Fgiraffe%2Fvoucher.html%3FcampType%3DPROMO_KERNEL%26campId%3D485a72744c7479626d747473724552333646443476413d3d%26voucherTemplateId%3D71327a6c533965684774544339735043484a4c523337655173675272784359364f586951765a4a6f3659493d%26requestFrom%3DSELF_MARKET%26chInfo%3DSHARE_LINK',
    //   complete: res => {
    //     console.log(res)
    //   }
    // })
    // my.ap.navigateToAlipayPage({
    //   path: 'https://render.alipay.com/p/s/i/?scheme=alipays%3A%2F%2Fplatformapi%2Fstartapp%3FappId%3D20000067%26url%3Dhttps%253A%252F%252Frender.alipay.com%252Fp%252Fs%252Fgiraffe%252Fvoucher.html%253FcampType%253DPROMO_KERNEL%2526campId%253D485a72744c7479626d747473724552333646443476413d3d%2526voucherTemplateId%253D71327a6c533965684774544339735043484a4c523337655173675272784359364f586951765a4a6f3659493d%2526requestFrom%253DSELF_MARKET%2526chInfo%253DSHARE_LINK',
    //   complete: res => {
    //     console.log(res)
    //   }
    // })

    // my.ap.openURL({
    //   url: `https://render.alipay.com/p/s/i?appId=20000067&url=https%3A%2F%2Fds.alipay.com%2Ferror%2FredirectLink.htm%3Furl%3D${encodeURIComponent('ttuki://app/')}`,
    //   complete: res => {
    //     console.log(111, res)
    //   }
    // })
    // my.openOtherApp({
    //   url: 'ttuki://app/',
    //   complete: res => {
    //     console.log(res)
    //   }
    // })
    // my.ap.openURL({
    //   url: `https://render.alipay.com/p/s/i?appId=20000067&url=https%3A%2F%2Fds.alipay.com%2Ferror%2FredirectLink.htm%3Furl%3D${encodeURIComponent('com.qts.customer://')}`,
    //   complete: res => {
    //     console.log(111, res)
    //   }
    // })
  },
  handleClose() {
    my.exitMiniProgram()
  },

  onShow() {
    if (typeof this.getTabBar === "function" && this.getTabBar()) {
      this.getTabBar().setData({
        selected: 0
      });
    }
  },

  randomId() {
    let list = [];
    for (let index = 100; index < 1000; index++) {
      list.push(index);
    }
    this.setData({
      list
    });
  },

  handlelifeStyle() {
    // let path = `https://render.alipay.com/p/s/i/?scheme=${encodeURIComponent('alipays://platformapi/startapp?appId=20000042&publicBizType=LIFE_APP&publicId=2015111300783988')}`
    let path = `https://render.alipay.com/p/s/i/?appId=20000042`
    my.ap.navigateToAlipayPage({
      path,
      complete: res => {
        console.log(res)
      }
    });
  },

  handleClickApp() {
    my.openOtherApp({
      url: 'weixin://dl/business/?t=EPBNL6dNM8r'
    })
  },

  handleClick() {
    let path = `https://render.alipay.com/p/s/i/?appId=10000007&clientVersion=3.7.0.0718&qrcode=${encodeURIComponent('https://ds.alipay.com/error/redirectLink.htm?url=com.qts.jianzhixia://linkedme?click_id=au5qSd2xN')}`
    my.ap.navigateToAlipayPage({
      path
    })
  },

  handleClick0() {
    let path = "https://render.alipay.com/p/s/i/?appId=2021001141626787&query=from%253Dqingtuanshe%2526jumpUrl%253Dhttps%25253A%25252F%25252Fpage.cainiao.com%25252Fmcn%25252Fcainiao-town%25252Findex.html%25253FdisableNav%25253DYES%252526__webview_options__%25253DfullScreen%2525253DYES%25252526fullscreen%2525253DYES%25252526showStateBar%2525253DYES%252526%252523%25252F%25253Fsource%25253Dqingtuanshe"
    my.ap.navigateToAlipayPage({
      path,
      success: res => {
        console.log(res);
      },
      fail: error => { }
    });
  },

  handleClick1() {
    let path = `https://render.alipay.com/p/s/i/?appId=10000007&clientVersion=3.7.0.0718&qrcode=${"https://m.jd.com"}`;
    my.ap.navigateToAlipayPage({
      path,
      success: res => {
        console.log(res);
      },
      fail: error => { }
    });
  },

  handleClick2() {
    let path = `https://render.alipay.com/p/s/i/?appId=20001003&keyword=青团社&v2=true`;
    my.ap.navigateToAlipayPage({
      path,
      success: res => {
        console.log(res);
      },
      fail: error => { }
    });
  },

  handleClick3() {
    let path = `https://render.alipay.com/p/s/i/?appId=10000007&clientVersion=3.7.0.0718&qrcode=${"https://m.tb.cn/h.U58pzuH?tk=tLnkd05ABDJ"}`;
    my.ap.navigateToAlipayPage({
      path,
      success: res => {
        console.log(res);
      },
      fail: error => { }
    });
  },

  handleClick4(e) {
    let url = `https://m.taobao.com`;
    let longUrl = `https://m.qtshe.com/app/webview?targetUrl=${encodeURIComponent(
      url
    )}`;
    my.navigateTo({
      url: `/pages/webview/index?url=${encodeURIComponent(longUrl)}&isBack=true`
    });
  },
  // 打开语雀小程序再次打开小程序内的h5链接
  handleClick5(e) {
    my.call("pushWindow", {
      url:
        "alipays://platformapi/startapp?appId=68687807&url=https%3A%2F%2Fqtshe.yuque.com%2Fertegq%2Fpyhafh%2Fqiifgf"
    });
  },
  // 打开生活号文章
  handleClick6(e) {
    // let path = `https://render.alipay.com/p/h5/life_public/www/index.html?publicId=2015111300783988&msgId=2015111300783988059d1e5c-ff08-4e40-b5ca-73f2ba2f1d05`;
    // my.ap.navigateToAlipayPage({
    //   path
    // });
    // let path = `https://render.alipay.com/p/s/i/?appId=20000909&url=${encodeURIComponent(encodeURIComponent('/www/msg.html?publicId=2015111300783988&msgId=2015111300783988059d1e5c-ff08-4e40-b5ca-73f2ba2f1d05'))}`
    // my.ap.navigateToAlipayPage({
    //   path
    // })
    let url = `alipays://platformapi/startapp?appId=20000909&url=${encodeURIComponent('/www/msg.html?publicId=2015111300783988&msgId=2015111300783988059d1e5c-ff08-4e40-b5ca-73f2ba2f1d05')}`
    my.call("pushWindow", {
      url
    })
  },
  // 打开生活号后再次打开一条h5链接
  handleClick7(e) {
    // my.navigateToMiniProgram({
    //   appId: '2015111300783988' // 芝麻信用生活号。2021002172648224:芭芭农场生活号
    // })
    let url = "https://m.qtshe.com";
    // alipays://platformapi/startapp?appId=20000042&publicId=2015111300783988&url=你想打开的h5地址
    // let path = `https://render.alipay.com/p/s/i/?appId=20000042&publicId=2015111300783988&url=${encodeURIComponent(url)}`
    // my.ap.navigateToAlipayPage({
    //   path
    // });

    my.call("pushWindow", {
      url: `alipays://platformapi/startapp?appId=20000042&publicBizType=LIFE_APP&publicId=2015111300783988&url=${encodeURIComponent(
        url
      )}`
    });
  },
  // 打开生活号内的某条视频
  handleClick8(e) {
    let path = `https://render.alipay.com/p/s/i/?appId=68687748&url=%2Fwww%2Fdetail.html%3FcontentId%3D20220710OB020010031908638746`;
    my.ap.navigateToAlipayPage({
      path
    });
  },

  handleClick9() {
    let path = `https://render.alipay.com/p/s/i/?appId=20000067&url=${'https%253A%252F%252Frender.alipay.com%252Fp%252Fs%252Fgiraffe%252Fvoucher.html%253FcampType%253DPROMO_KERNEL%2526campId%253D6636365f78426b786f625a6936546e7254395f4d39673d3d%2526voucherTemplateId%253D554f337a6d3554453238736e374c5430694d734175365775483748526371466647613242536337666a61303d%2526requestFrom%253DSELF_MARKET'}`;
    my.ap.navigateToAlipayPage({
      path
    });
  },
  handleClick10() {
    let path = `https://render.alipay.com/p/s/i/?appId=68687809&source=qingtuanshe&url=%2Fwww%2Fgame.html`;
    my.ap.navigateToAlipayPage({
      path
    });
  },
  handleClick11() {
    my.ap.navigateToAlipayPage({
      path:
        "https://render.alipay.com/p/yuyan/180020010000712015/instradem.html?spNo=24859&identity=work&entrance=cx_yiwai_qingtuanshe_banner"
    });
  }
});
