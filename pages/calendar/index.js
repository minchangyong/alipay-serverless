Page({
  data: {
    visible: false,
    monthTranslate: {
      4: 1,
      6: 1,
      12: 0
    }, //地图上的红蓝点
  },

  handleClick() {
    this.setData({
      visible: true
    })
  },
  handleClose() {
    this.setData({
      visible: false
    })
  },
  handleConfirm(e) {
    console.log(e.value)
    this.setData({
      selectTime: e.value
    })
  }
})