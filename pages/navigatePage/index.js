Page({
  data: {
    navList: [
      '打开蚂蚁新村小程序游戏页面',
      '打开网商银行活动页面',
      '打开外部领优惠券页',
      '打开芝麻工作证粽子盲盒活动',
      '打开蚂蚁保-灵活工作保',
      '打开生活号文章(1)',
      '打开生活号文章(2)',
      '打开生活号文章(3)',
      '打开青团社直播间',
      '打开斜杠课堂直播间',
      '打开生活号视频',
      '打开青团社兼职App'
    ]
  },
  handleClick(e) {
    const { dataset: { index = 0 } = {} } = e.currentTarget
    switch (index) {
      case 0:
        // 打开蚂蚁新村小程序游戏页面  `alipays://platformapi/startapp?appId=68687809&source=qingtuanshe&url=%2Fwww%2Fgame.html`
        my.ap.navigateToAlipayPage({
          path: `https://render.alipay.com/p/s/i/?appId=20000067&source=qingtuanshe&url=%2Fwww%2Fgame.html`
        })
        break
      case 1:
        // 打开网商银行活动页面 `https://render.mybank.cn/p/c/180020870000000531/index.html?trace=true&__webview_options__=applet%3Dfalse&scm=1.bkcdp.2022070700010100000055060453._._._._&chInfo=MYBANK_MULAN_QTS`
        my.ap.navigateToAlipayPage({
          path: `https://render.alipay.com/p/s/i/?appId=20000067&url=https%253A%252F%252Frender.mybank.cn%252Fp%252Fc%252F180020870000000531%252Findex.html%253Ftrace%253Dtrue%2526__webview_options__%253Dapplet%25253Dfalse%2526scm%253D1.bkcdp.2022070700010100000055060453._._._._%2526chInfo%253DMYBANK_MULAN_QTS`
        })
        break
      case 2:
        // 打开外部领优惠券页 `https://render.alipay.com/p/s/giraffe/voucher.html?campType=PROMO_KERNEL&campId=6f42746931505533386d3267795f72777430745638673d3d&voucherTemplateId=6b715568304f4565617a485267584d42613038486474616b716d6a432f5f3145726f694f7538744a3637453d&requestFrom=SELF_MARKET&chInfo=SHARE_LINK`
        my.ap.navigateToAlipayPage({
          path: `https://render.alipay.com/p/s/i/?appId=20000067&url=https%253A%252F%252Frender.alipay.com%252Fp%252Fs%252Fgiraffe%252Fvoucher.html%253FcampType%253DPROMO_KERNEL%2526campId%253D6f42746931505533386d3267795f72777430745638673d3d%2526voucherTemplateId%253D6b715568304f4565617a485267584d42613038486474616b716d6a432f5f3145726f694f7538744a3637453d%2526requestFrom%253DSELF_MARKET%2526chInfo%253DSHARE_LINK`
        })
        break
      case 3:
        // 参数2次encode
        // 芝麻工作证粽子盲盒活动 `alipays://platformapi/startapp?appId=68687748&url=%2Fwww%2Fchannel.html%3FenableWK%3DYES%26topicId%3DBC_TP_20220531000451686%26chInfo%3Dch_B007__chsub_HTopic&pullRefresh=YES&sd=NO&ttb=auto`
        my.ap.navigateToAlipayPage({
          path: `https://render.alipay.com/p/s/i/?appId=68687748&url=%252Fwww%252Fchannel.html%253FenableWK%253DYES%2526topicId%253DBC_TP_20220531000451686%2526chInfo%253Dch_B007__chsub_HTopic%2526enableWK%253DYES%26appClearTop%3Dfalse%26sd%3DNO%26startMultApp%3DYES&chInfo=ch_share__chsub_CopyLink&apshareid=8356348D-BF2C-4420-AE5D-66696964D260`
        })
        break
      case 4:
        // 蚂蚁保-灵活工作保 `alipays://platformapi/startapp?appId=20000067&url=https%3A%2F%2Frender.alipay.com%2Fp%2Fyuyan%2F180020010000712015%2Finstradem.html%3FspNo%3D24859%26identity%3Dwork%26entrance%3Dcx_yiwai_qingtuanshe_banner`
        my.ap.navigateToAlipayPage({
          path: `https://render.alipay.com/p/s/i/?appId=20000067&url=https%3A%2F%2Frender.alipay.com%2Fp%2Fyuyan%2F180020010000712015%2Finstradem.html%3FspNo%3D24859%26identity%3Dwork%26entrance%3Dcx_yiwai_qingtuanshe_banner`
        })
        break
      case 5:
        // 打开生活号文章 `https://render.alipay.com/p/h5/life_public/www/index.html?publicId=2021001165600178&msgId=2021001165600178ed1b3dc9-d444-4841-af90-891775ad462b`
        my.ap.navigateToAlipayPage({
          path: `https://render.alipay.com/p/s/i/?appId=20000067&url=https%253A%252F%252Frender.alipay.com%252Fp%252Fh5%252Flife_public%252Fwww%252Findex.html%253FpublicId%253D2021001165600178%2526msgId%253D2021001165600178ed1b3dc9-d444-4841-af90-891775ad462b`
        })
        break
      case 6:
        // 打开生活号文章 `alipays://platformapi/startapp?appId=20000909&url=%2Fwww%2Findex.html%3FpublicId%3D2021001165600178%26msgId%3D2021001165600178ed1b3dc9-d444-4841-af90-891775ad462b%26cardId%3D2021001165600178916064af-7faa-47d7-830e-c80389e8d880`
        my.ap.navigateToAlipayPage({
          path: `https://render.alipay.com/p/s/i/?appId=20000909&url=%252Fwww%252Fmsg.html%253FpublicId%253D2021001165600178%2526msgId%253D2021001165600178ed1b3dc9-d444-4841-af90-891775ad462b%2526cardId%253D2021001165600178916064af-7faa-47d7-830e-c80389e8d880`
        })
        break
      case 7:
        // 打开生活号文章 `alipays://platformapi/startapp?appId=20000909&url=%2Fwww%2Findex.html%3FpublicId%3D2021001165600178%26msgId%3D2021001165600178ed1b3dc9-d444-4841-af90-891775ad462b%26cardId%3D2021001165600178916064af-7faa-47d7-830e-c80389e8d880`
        let url = `alipays://platformapi/startapp?appId=20000909&url=${encodeURIComponent('/www/msg.html?publicId=2021001165600178&msgId=2021001165600178ed1b3dc9-d444-4841-af90-891775ad462b&cardId=2021001165600178916064af-7faa-47d7-830e-c80389e8d880')}`
        my.call('pushWindow', {
          url
        })
        break
      case 8:
        // 打开青团社直播间 `alipays://platformapi/startapp?appId=2021001152663226&page=pages%2Flive-room%2Flive-room%3Fid%3D361859010850%26userId%3D4173090392%26liveSource%3Dshh_backstage`
        // my.ap.navigateToAlipayPage({
        //   path: `https://render.alipay.com/p/s/i/?appId=2021001152663226&page=pages%2Flive-room%2Flive-room%3Fid%3D361859010850%26userId%3D4173090392%26liveSource%3Dshh_backstage`
        // })
        my.navigateToMiniProgram({
          path: 'pages/live-room/live-room?id=361859010850&userId=4173090392&liveSource=shh_backstage',
          appId: '2021001152663226'
        })
        break
      case 9:
        // 打开斜杠课堂直播间 `alipays://platformapi/startapp?appId=20002092&liveId=A202207210151069501001799&liveSource=shh_backstage`
        my.ap.navigateToAlipayPage({
          path: `https://render.alipay.com/p/s/i/?appId=20002092&liveId=A202207210151069501001799&liveSource=shh_backstage`
        })
        break
      case 10:
        // 打开生活号视频 `alipays://platformapi/startapp?appId=68687748&url=%2Fwww%2Fdetail.html%3FcontentId%3D20220710OB020010031908638746%26refer%3Dsingle%26chInfo%3DcontentScheme%26lifeContentId%3D20151113007839880eb62601-d587-4688-b7fe-3932db50bab5&enableWK=YES&ttb=always&pd=NO&titlePenetrate=YES&bc=0&enableComponentOverlayer=YES&appClearTop=false&startMultApp=YES&needRedirect=true&contentId=20220710OB020010031908638746&refer=single&chInfo=contentScheme`
        my.ap.navigateToAlipayPage({
          path: `https://render.alipay.com/p/s/i/?appId=68687748&url=%2Fwww%2Fdetail.html%3FcontentId%3D20220710OB020010031908638746%26refer%3Dsingle%26chInfo%3DcontentScheme%26lifeContentId%3D20151113007839880eb62601-d587-4688-b7fe-3932db50bab5&enableWK=YES&ttb=always&pd=NO&titlePenetrate=YES&bc=0&enableComponentOverlayer=YES&appClearTop=false&startMultApp=YES&needRedirect=true&contentId=20220710OB020010031908638746&refer=single&chInfo=contentScheme`
        })
        break
      case 11:
        // 打开青团社兼职App
        my.ap.navigateToAlipayPage({
          path: `https://render.alipay.com/p/s/i/?appId=10000007&clientVersion=3.7.0.0718&qrcode=${"https://m.qtshe.com/app/openQtsApp"}`
        })
        break
      default:
        my.ap.navigateToAlipayPage({
          path: `https://render.alipay.com/p/s/i/?appId=68687748&url=%2Fwww%2Fdetail.html%3FcontentId%3D20220710OB020010031908638746%26refer%3Dsingle%26chInfo%3DcontentScheme%26lifeContentId%3D20151113007839880eb62601-d587-4688-b7fe-3932db50bab5&enableWK=YES&ttb=always&pd=NO&titlePenetrate=YES&bc=0&enableComponentOverlayer=YES&appClearTop=false&startMultApp=YES&needRedirect=true&contentId=20220710OB020010031908638746&refer=single&chInfo=contentScheme`
        })
    }
  }
});
