Page({
  data: {},
  onLoad() {
    my.getImageInfo({
      src: 'https://qiniu-image.qtshe.com/IMG_20230127_120452.jpg',
      success: res => {
        my.compressImage({
          apFilePaths: [res.path],
          compressLevel: 1,
          success: (resp) => {
            console.log(resp)
            my.saveImageToPhotosAlbum({
              filePath: resp.apFilePaths[0],
              complete: res => {
                console.log(res)
              }
            })
          }
        })
      }
    })

  },
});
