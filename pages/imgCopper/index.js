Page({
  data: {
    imageSrc: ''
  },

  onLoad(options) {
    console.log(options)
  },

  onShow() {
    if (typeof this.getTabBar === "function" && this.getTabBar()) {
      this.getTabBar().setData({
        selected: 1
      });
    }
  },
  handleChooseImg() {
    my.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: (todo) => {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        let tempFilePaths = todo.apFilePaths[0];
        this.setData({
          imageSrc: tempFilePaths
        })
      }
    })
  },
  handleSuccess(e) {
    console.log(e)
    this.setData({
      headImg: e.path
    })
  },
  handleError(e) {
    console.log(e)
  }
})