Component({
  mixins: [],
  data: {
    
  },
  props: {
    currentTab: 0, // 当前点击下标
    list: [], // 导航条列表， redDot配置需加入badge字段。如： tabList: [{ name: '有趣好玩', badge: 20}]
    color: '#00cc88' // 文字及导航条下标
  },
  methods: {
    // 导航条点击切换
    handleClick(e) {
      let currentTab = e.currentTarget.dataset.index
      this.setData({
        currentTab
      })
    },
    // 触动swiper滑动切换导航条
    handleSwiper(e) {
      console.log(e)
      let {
        current,
        source
      } = e.detail
      if (source === 'autoplay' || source === 'touch') {
        const currentTab = current
        this.setData({
          currentTab
        })
      }
    }
  },
});
