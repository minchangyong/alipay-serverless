var windowWRPX = 750
// 拖动时候的 pageX
var pageX = 0
// 拖动时候的 pageY
var pageY = 0
// 获取手机像素值
var pixelRatio = my.getSystemInfoSync().pixelRatio
// 调整大小时候的 pageX
var sizeConfPageX = 0
// 调整大小时候的 pageY
var sizeConfPageY = 0

var initDragCutW = 0
var initDragCutL = 0
var initDragCutH = 0
var initDragCutT = 0

// 移动时 手势位移与 实际元素位移的比
var dragScaleP = 2
var isSrcLoad = true
Component({
  data: {
    visible: false,  // 是否现实裁切页面
    isShowImg: false,
    // 初始化的宽高
    cropperInitW: windowWRPX,
    cropperInitH: windowWRPX,
    // 动态的宽高
    cropperW: windowWRPX,
    cropperH: windowWRPX,
    // 动态的left top值
    cropperL: 0,
    cropperT: 0,

    // 图片缩放值
    scaleP: 0,
    imageW: 0,
    imageH: 0,

    // 裁剪框 宽高
    cutW: 400,
    cutH: 400,
    cutL: 0,
    cutT: 0,
  },
  props: {
    src: '', // 需要裁切的图片
  },
  didUpdate() {
    if (isSrcLoad && this.props.src) {
      isSrcLoad = false
      this.startCropper()
    }
  },
  methods: {
    // 拖动时候触发的touchStart事件
    contentStartMove(e) {
      pageX = e.touches[0].pageX
      pageY = e.touches[0].pageY
    },
    // 拖动时候触发的touchMove事件
    contentMoveing(e) {
      let dragLengthX = (pageX - e.touches[0].pageX) * dragScaleP
      let dragLengthY = (pageY - e.touches[0].pageY) * dragScaleP
      let minX = Math.max(this.data.cutL - (dragLengthX), 0)
      let minY = Math.max(this.data.cutT - (dragLengthY), 0)
      let maxX = this.data.cropperW - this.data.cutW
      let maxY = this.data.cropperH - this.data.cutH
      this.setData({
        cutL: Math.min(maxX, minX),
        cutT: Math.min(maxY, minY),
      })
      pageX = e.touches[0].pageX
      pageY = e.touches[0].pageY
    },
    // 设置大小的时候触发的touchStart事件
    dragStart(e) {
      sizeConfPageX = e.touches[0].pageX
      sizeConfPageY = e.touches[0].pageY
      initDragCutW = this.data.cutW
      initDragCutL = this.data.cutL
      initDragCutT = this.data.cutT
      initDragCutH = this.data.cutH
    },
    // 设置大小的时候触发的touchMove事件
    dragMove(e) {
      let dragType = e.target.dataset.drag
      switch (dragType) {
        case 'right':
          var dragLength = (sizeConfPageX - e.touches[0].pageX) * dragScaleP
          if (initDragCutW >= dragLength) {
            // 如果 移动小于0 说明是在往下啦  放大裁剪的高度,图片的高度  最大等于图片的top值加当前图片的高度,否则就说明超出界限
            if (dragLength < 0 && this.data.cropperW > initDragCutL + this.data.cutW) {
              this.setData({
                cutW: initDragCutW - dragLength
              })
            }
            // 如果是移动 大于0  说明在缩小  只需要缩小的距离小于原本裁剪的高度就ok
            if (dragLength > 0) {
              this.setData({
                cutW: initDragCutW - dragLength
              })
            }
            else {
              return
            }
          } else {
            return
          }
          break;
        case 'left':
          var dragLength = (dragLength = sizeConfPageX - e.touches[0].pageX) * dragScaleP
          if (initDragCutW >= dragLength && initDragCutL > dragLength) {
            if (dragLength < 0 && Math.abs(dragLength) >= initDragCutW) return
            this.setData({
              cutL: initDragCutL - dragLength,
              cutW: initDragCutW + dragLength
            })
          } else {
            return;
          }
          break;
        case 'top':
          var dragLength = (sizeConfPageY - e.touches[0].pageY) * dragScaleP
          if (initDragCutH >= dragLength && initDragCutT > dragLength) {
            if (dragLength < 0 && Math.abs(dragLength) >= initDragCutH) return
            this.setData({
              cutT: initDragCutT - dragLength,
              cutH: initDragCutH + dragLength
            })
          } else {
            return;
          }
          break;
        case 'bottom':
          var dragLength = (sizeConfPageY - e.touches[0].pageY) * dragScaleP
          // 必须是 dragLength 向上缩小的时候必须小于原本的高度
          if (initDragCutH >= dragLength) {
            // 如果移动小于0,说明是在往下拉. 放大裁剪的高度,最大等于图片的top值加当前图片的高度,否则就说明超出界限
            if (dragLength < 0 && this.data.cropperH > initDragCutT + this.data.cutH) {
              this.setData({
                cutH: initDragCutH - dragLength
              })
            }
            // 如果是移动 大于0  说明在缩小  只需要缩小的距离小于原本裁剪的高度就ok
            if (dragLength > 0) {
              this.setData({
                cutH: initDragCutH - dragLength
              })
            }
            else {
              return
            }
          } else {
            return
          }
          break;
        case 'rightBottom':
          var dragLengthX = (sizeConfPageX - e.touches[0].pageX) * dragScaleP
          var dragLengthY = (sizeConfPageY - e.touches[0].pageY) * dragScaleP
          if (initDragCutH >= dragLengthY && initDragCutW >= dragLengthX) {
            // bottom 方向的变化
            if ((dragLengthY < 0 && this.data.cropperH > initDragCutT + this.data.cutH) || (dragLengthY > 0)) {
              this.setData({
                cutH: initDragCutH - dragLengthY
              })
            }

            // right 方向的变化
            if ((dragLengthX < 0 && this.data.cropperW > initDragCutL + this.data.cutW) || (dragLengthX > 0)) {
              this.setData({
                cutW: initDragCutW - dragLengthX
              })
            }
            else {
              return
            }
          } else {
            return
          }
          break;
        default:
          break;
      }
    },
    // 获取图片信息, 设置画布情况
    startCropper() {
      // start
      my.getImageInfo({
        src: this.props.src,
        success: (res) => {
          let innerAspectRadio = res.width / res.height;
          // 根据图片的宽高显示不同的效果   保证图片可以正常显示
         if (innerAspectRadio > 1) {
            this.setData({
              cropperW: windowWRPX,
              cropperH: windowWRPX / innerAspectRadio,
              // 初始化left right
              cropperL: Math.ceil((windowWRPX - windowWRPX) / 2),
              cropperT: Math.ceil((windowWRPX - windowWRPX / innerAspectRadio) / 2),
              // 裁剪框  宽高 
              // cutW: windowWRPX - 200,
              // cutH: windowWRPX / innerAspectRadio - 200,
              cutL: Math.ceil((windowWRPX - windowWRPX + 340) / 2),
              cutT: Math.ceil((windowWRPX / innerAspectRadio - (windowWRPX / innerAspectRadio - 20)) / 2),
              // 图片缩放值
              scaleP: res.width * pixelRatio / windowWRPX,
              // 图片原始宽度 rpx
              imageW: res.width * pixelRatio,
              imageH: res.height * pixelRatio
            })
          } else {
            this.setData({
              cropperW: windowWRPX * innerAspectRadio,
              cropperH: windowWRPX,
              // 初始化left right
              cropperL: Math.ceil((windowWRPX - windowWRPX * innerAspectRadio) / 2),
              cropperT: Math.ceil((windowWRPX - windowWRPX) / 2),
              // 裁剪框的宽高
              // cutW: windowWRPX * innerAspectRadio - 66,
              // cutH: 400,
              cutL: Math.ceil((windowWRPX * innerAspectRadio - (windowWRPX * innerAspectRadio - 20)) / 2),
              cutT: Math.ceil((windowWRPX - 340) / 2),
              // 图片缩放值
              scaleP: res.width * pixelRatio / windowWRPX,
              // 图片原始宽度 rpx
              imageW: res.width * pixelRatio,
              imageH: res.height * pixelRatio
            })
          }
          this.setData({
            isShowImg: true,
            visible: true
          })
        }
      })
    },
    //点击取消图片
    handleCancel() {
      this.setData({
        visible: false
      }, () => {
        isSrcLoad = true
      })
    },
    // 点击完成图片
    handleConfirm() {
      my.showLoading({
        title: '图片生成中...',
      })
      // 将图片写入画布             
      const ctx = my.createCanvasContext('myCanvas')
      ctx.drawImage(this.props.src, 0, 0, this.data.imageW / pixelRatio, this.data.imageH / pixelRatio)
      var canvasW = (this.data.cutW / this.data.cropperW) * (this.data.imageW / pixelRatio)
      var canvasH = (this.data.cutH / this.data.cropperH) * (this.data.imageH / pixelRatio)
      var canvasL = (this.data.cutL / this.data.cropperW) * (this.data.imageW / pixelRatio)
      var canvasT = (this.data.cutT / this.data.cropperH) * (this.data.imageH / pixelRatio)
      ctx.draw(true, () => {
        ctx.toTempFilePath({
          x: canvasL,
          y: canvasT,
          width: canvasW,
          height: canvasH,
          destWidth: canvasW,
          destHeight: canvasH,
          success: (res) => {
            my.hideLoading()
            // 判断时上传头像还是二维码
            this.setData({
              visible: false
            }, () => {
              isSrcLoad = true
            })
            this.props.onSuccess({ path: res.apFilePath })
          },
          fail: err => {
            my.hideLoading()
            my.showToast({
              content: '裁剪图片失败'
            })
            this.setData({
              visible: false
            }, () => {
              isSrcLoad = true
            })
            this.props.onError({ errMsg: err.errMsg })
          }
        })
      })
    }
  }
})