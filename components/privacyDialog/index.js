// 组件js
let privacyHandler
let privacyResolves = new Set()
let closeOtherPageshowDialogHooks = new Set()

if (my.onNeedPrivacyAuthorization) {
  my.onNeedPrivacyAuthorization(resolve => {
    if (typeof privacyHandler === 'function') {
      privacyHandler(resolve)
    }
  })
}

const closeOtherPageshowDialog = (closeDialog) => {
  closeOtherPageshowDialogHooks.forEach(hook => {
    if (closeDialog !== hook) {
      hook()
    }
  })
}

Component({
  data: {
    visible: false,
    mini_logo: my.getAccountInfoSync().miniProgram.logo,
    mini_name: my.getAccountInfoSync().miniProgram.name
  },
  props: {
    useSafeArea: { // 是否在非tabBar页面引用，适配tabbar底部      type: Boolean,
      value: true
    }
  },
  lifetimes: {
    attached() {
      const closeDialog = () => {
        this.hideDialog()
      }
      privacyHandler = resolve => {
        privacyResolves.add(resolve)
        this.showDialog()
        // 额外逻辑：当前页面的隐私弹窗弹起的时候，关掉其他页面的隐私弹窗
        closeOtherPageshowDialog(closeDialog)
      }
      this.closeDialog = closeDialog
      closeOtherPageshowDialogHooks.add(closeDialog)
    },
    detached() {
      closeOtherPageshowDialogHooks.delete(this.closeDialog)
    }
  },
  pageLifetimes: {
  // 解决首页有手机授权、进入二级页面再返回就无法弹出隐私协议的问题。
    show() {
      if (this.closeDialog) {
        privacyHandler = resolve => {
          privacyResolves.add(resolve)
          this.showDialog()
          // 额外逻辑：当前页面的隐私弹窗弹起的时候，关掉其他页面的隐私弹窗
          closeOtherPageshowDialog(this.closeDialog)
        }
      }
    }
  },
  methods: {
    handleAgree() {
      this.hideDialog()
      // 这里同时调用多个wx隐私接口：让隐私弹窗保持单例，点击一次同意按钮即可让所有pending中的wx隐私接口继续执行
      privacyResolves.forEach(resolve => {
        resolve({
          event: 'agree',
          buttonId: 'agree-btn'
        })
      })
      privacyResolves.clear()
    },
    // 拒绝协议
    handleDisagree(e) {
      this.hideDialog()
      privacyResolves.forEach(resolve => {
        resolve({
          event: 'disagree'
        })
      })
      privacyResolves.clear()
    },
    // 显示隐私协议弹窗
    showDialog() {
      if (!this.data.visible) {
        this.setData({
          visible: true
        })
        // 控制底部页面不可滚动
        my.setPageStyle({
          style: {
            overflow: 'hidden'
          }
        })
      }
    },
    // 隐藏隐私协议弹窗
    hideDialog() {
      if (this.data.visible) {
        this.setData({
          visible: false
        })
        // 控制底部页面可以滚动
        my.setPageStyle({
          style: {
            overflow: 'auto'
          }
        })
      }
    },
    // 跳转隐私协议
    openPrivacyContract() {
      my.openPrivacyContract({
        success: res => {
          console.log('openPrivacyContract success', res)
        },
        fail: err => {
          console.error('openPrivacyContract fail', err)
        }
      })
    },
    // tabbar页面切换时关掉其他页面的隐私协议弹窗
    tabBarPageShow() {
      this.handleDisagree() // 切换tabbar默认用户点了拒绝，保持单例
      if (this.closeDialog) {
        privacyHandler = resolve => {
          privacyResolves.add(resolve)
          this.showDialog()
          // 额外逻辑：当前页面的隐私弹窗弹起的时候，关掉其他页面的隐私弹窗
          closeOtherPageshowDialog(this.closeDialog)
        }
      }
    }
  }
})